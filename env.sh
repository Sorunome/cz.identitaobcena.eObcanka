#!/bin/sh
LD_LIBRARY_PATH=/app/eObcanka/lib/openssl1.1:/app/eObcanka/lib/qt5.15/lib:/app/eObcanka/lib:/app/lib:$LD_LIBRARY_PATH

if [[ $(cat /proc/version) =~ "fedora" || $(cat /proc/version) =~ "Red Hat" ]]; then
  echo "fedora"
  LD_LIBRARY_PATH=/app/lib/pcsc-lite-fedora:$LD_LIBRARY_PATH
else
  echo "normal"
  LD_LIBRARY_PATH=/app/lib/pcsc-lite:$LD_LIBRARY_PATH
fi

echo $LD_LIBRARY_PATH
export LD_LIBRARY_PATH

export QT_PLUGIN_PATH=/app/eObcanka/lib/qt5.15/plugins:$QT_PLUGIN_PATH
