#!/bin/sh

find=$(mktemp)
printf $1 | hexdump -Cv > $find
replace=$(mktemp)
printf $2 | hexdump -Cv > $replace
file=$(mktemp)
infile=$3
hexdump -Cv $infile > $file

array=( $find $replace $file )

for i in "${array[@]}"; do
  # remove line numbers
  sed 's/\w\{5,\}//g' -i $i
  # remove the suffix thing
  sed 's/|.\{,16\}|//g' -i $i
  # remove ALL whitespace
  sed ':a;N;$!ba;s/\s//g' -i $i
done
# do the replacement on the hex
sed "s/$(cat $find)/$(cat $replace)/gi" -i $file

# convert from hex to bin
printf "$(cat $file | sed 's/\([0-9a-f]\{2\}\)\([0-9a-f]\{2\}\)/\\x\1\\x\2/g')" > $infile
chmod +x $infile

rm $file $find $replace
